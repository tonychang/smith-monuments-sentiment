PORT=8080
IP=0.0.0.0
nvidia-docker run -it \
    -p $PORT:$PORT \
    -v $(pwd):/contents \
    -v /datadrive:/data \
    -w /contents --rm \
    tonychangcsp/fastai:v070  \
        jupyter notebook \
            --port $PORT \
            --ip $IP \
            --no-browser --allow-root

