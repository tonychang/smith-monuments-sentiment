from selenium import webdriver
import time
from bs4 import BeautifulSoup as bs
import pandas as pd
import json
import numpy as np 
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("--start_num", help="first comment number to search",\
        type=int)
parser.add_argument("--end_num", help="last comment number to search",\
        type=int)
parser.add_argument("--output_folder", help="folder to output to",\
        type=str)
args = parser.parse_args()

chrome_options = webdriver.ChromeOptions()
chrome_options.add_argument("--no-sandbox")
chrome_options.add_argument("--headless")
chrome_options.add_argument("--disable-gpu")
chrome_options.add_argument("--disable-dev-shm-usage")

driver = webdriver.Chrome(chrome_options=chrome_options)
doc_list = np.arange(args.start_num,args.end_num+1)
columns = ['id', 'date']
data = []
delay = 0.25
for doc_num in doc_list:
    url = "%s%04d"%("https://www.regulations.gov/document?D=DOI-2017-0002-",doc_num)
    try:
        driver.get(url)
        time.sleep(delay)
        #driver.switch_to.frame(driver.find_element_by_tag_name("iframe"))
        html = driver.page_source
        soup = bs(html, "html.parser")
        for d in soup.find_all("span", attrs={"class":"breakWord","style":"display: block;"}):
            date = d.text
        data.append(["DOI-2017-0002-%04d"%doc_num, date])
        #below code for capturing date as well....
        #for d in soup.find_all("span", attrs={"class":"breakWord","style":"display: block;"}):  
        #    post_date = d.text
        #data.append(["DOI-2017-0002-%04d"%doc_num, cmt.text, post_date,url])
    except:
        continue

out = pd.DataFrame(data, columns=columns)
outcsv = "%s/dates_%s_%s.csv"%(args.output_folder,args.start_num+1, args.end_num)
out.to_csv(outcsv, index=False)
#outjson = "/contents/data/comments.json"
#out.to_json(outjson)
