import re
import html
import numpy as np
from fastai.text import Tokenizer
from fastai.text import partition_by_cores
import pandas as pd
import difflib
import nltk
import en_core_web_sm

nlp = en_core_web_sm.load()

re1 = re.compile(r'  +')

BOS = 'xbos'
FLD = 'xfld'

def get_states_df():
    states = ["AL", "AK", "AZ", "AR", "CA", "CO", "CT", "DC", "DE", "FL", "GA", 
          "HI", "ID", "IL", "IN", "IA", "KS", "KY", "LA", "ME", "MD", 
          "MA", "MI", "MN", "MS", "MO", "MT", "NE", "NV", "NH", "NJ", 
          "NM", "NY", "NC", "ND", "OH", "OK", "OR", "PA", "RI", "SC", 
          "SD", "TN", "TX", "UT", "VT", "VA", "WA", "WV", "WI", "WY"]
    states_full = ['Alabama','Alaska','Arizona','Arkansas','California','Colorado','Connecticut','District of Columbia','Delaware','Florida','Georgia','Hawaii','Idaho','Illinois','Indiana','Iowa','Kansas','Kentucky','Louisiana','Maine', 'Maryland','Massachusetts','Michigan','Minnesota','Mississippi', 'Missouri','Montana','Nebraska','Nevada','New Hampshire','New Jersey','New Mexico','New York','North Carolina','North Dakota','Ohio','Oklahoma','Oregon','Pennsylvania','Rhode Island','South Carolina','South Dakota','Tennessee','Texas','Utah','Vermont','Virginia','Washington','West Virginia','Wisconsin','Wyoming']
<<<<<<< HEAD
    geostates = pd.read_csv('/data/smith-monuments-sentiment/data/states.csv')
=======
    geostates = pd.read_csv('./data/states.csv')
>>>>>>> 46242e3872d7b460a901fd472723c90a89b454c0
    return(pd.merge(pd.DataFrame({'State':states_full, 'Abbr':states}), geostates))

def get_nm_df():
    nm = [('Bears Ears', 'Utah'),('Grand Staircase-Escalante', 'Utah'),('Canyons of the Ancients', 'Colorado'),('Craters of the Moon', 'Idaho'),('Grand Canyon-Parashant', 'Arizona'),('Hanford Reach', 'Washington'),('Sand to Snow', 'California'),('Upper Missouri River Breaks', 'Montana'),('Basin and Range', 'Nevada'),('Berryessa Snow Mountain', 'California'),('Carrizo Plain', 'California'),('Cascade Siskiyou', 'Oregon'),('Giant Sequoia', 'California'),('Gold Butte', 'Nevada'),('Ironwood Forest', 'Arizona'),('Katahdin Woods and Waters', 'Maine'),('Marianas Trench', 'Northern Mariana Islands/Pacific Ocean'),('Mojave Trails', 'California'),('Northeast Canyons and Seamounts', 'Atlantic Ocean'),('Organ Mountains-Desert Peaks', 'New Mexico'),('Pacific Remote Islands', 'Pacific Ocean'),('Papahanaumokuakea', 'Hawaii'),('Rio Grande del Norte', 'New Mexico'),('Rose Atoll', 'American Samoa/Pacific Ocean'),('San Gabriel Mountains', 'California'),('Sonoran Desert', 'Arizona'),('Vermilion Cliffs', 'Arizona')]
    out = pd.DataFrame({'name':np.array(nm)[:,0],'region':np.array(nm)[:,1]})
    states_df = get_states_df()
    s = []
    for m in out['region']:
        ss = [v for k,v in zip(states_df.State,states_df.Abbr) if k==m]
        if ss:
            s.append(ss[0])
        else:
            s.append('Other')
    out['Abbr'] = s 
    return(out)

def natural_sort(df, col):
    df['_int'] = df[col].str.extract(r'(?:.*?\-){3}([^\/?#]+)(\d*)')[0]
    df['_int'] = df['_int'].astype(int)
    return df.sort_values(by=['_int']).drop(['_int'], axis=1)

def fixup(x):
    x = x.replace('#39;', "'").replace('amp;', '&').replace('#146;', "'").replace(
        'nbsp;', ' ').replace('#36;', '$').replace('\\n', "\n").replace('quot;', "'").replace(
        '<br />', "\n").replace('\\"', '"').replace('<unk>','u_n').replace(' @.@ ','.').replace(
        ' @-@ ','-').replace('\\', ' \\ ')
    return re1.sub(' ', html.unescape(x))

def get_texts(df, n_lbls=1):
    labels = df.iloc[:,range(n_lbls)].values.astype(np.int64)
    texts = f'\n{BOS} {FLD} 1 ' + df[n_lbls].astype(str)
    for i in range(n_lbls+1, len(df.columns)): texts += f' {FLD} {i-n_lbls} ' + df[i].astype(str)
    texts = list(texts.apply(fixup).values)

    tok = Tokenizer().proc_all_mp(partition_by_cores(texts))
    return tok, list(labels)

def get_all(df, n_lbls):
    tok, labels = [], []
    for i, r in enumerate(df):
        print(i)
        tok_, labels_ = get_texts(r, n_lbls)
        tok += tok_;
        labels += labels_
    return tok, labels

def find_word_coding(text, code='GPE'):
    doc = nlp(text)
    out = np.array([str(X) for X in doc.ents if X.label_ == code])
    if len(out) != 0:
        return(np.unique(out.flatten()))
    else:
        return(np.nan)

def check_nan(x):
    if isinstance(x,float):
       if np.isnan(x):
            return(True)
    return(False)

def check_gpe(gpe):
    if check_nan(gpe):
        return(['None'])   
    states_df = get_states_df()
    out = [str(element) for element in gpe if str(element) in list(states_df['Abbr']) or str(element) in list(states_df['State'])]
    if len(out)==0:
        return(['None'])
    return(out)

def check_org(org):
    if check_nan(org):
        return(['None'])   
    monuments_df = get_nm_df()
    out = [str(element) for element in org if str(element) in list(monuments_df['name'])]
    if len(out)==0:
        return(['None'])
    return(out)

def nan_finder(data):
    nans = np.ones(len(data)).astype('bool')
    for i in range(len(data)):
        try:
            if np.isnan(data[i]):
                next
        except:
            nans[i] = False
    return(nans)

def drop_list_dup(x):
    return list(dict.fromkeys(x))

def text_match(a,blist,min_len=7):
    m = np.zeros(len(blist))
    for i in range(len(blist)):
        matcher = difflib.SequenceMatcher(a=a, b=blist[i])
        m[i] = matcher.find_longest_match(0, len(matcher.a), 0,len(matcher.b)).size
    if np.max(m)<=min_len:
        return('None')
    return(blist[np.argmax(m)])

def parse_monuments(org, min_len=5):
    if check_nan(org):
        return(['None'])  
    monuments_df = get_nm_df()
    matches = [text_match(a,list(monuments_df['name']),min_len) for a in org]
    if len(matches)==0:
        return(['None'])
    out = drop_list_dup(matches)
    if len(out)>=2:
        return([x for x in out if x !='None'])
    return(out)

def extract_monuments(df):
    monuments_df = get_nm_df()
    mn_names = ['NM_%i'%i for i in range(len(monuments_df))]
    q = list(df[mn_names]==1)
    return(monuments_df[q])

def extract_states(df):
    states_df = get_states_df()
    st_names = [i for i in states_df.Abbr]
    q = list(df[st_names]==1)
    return(states_df[q])

def code_st_mn(df):
    mn = extract_monuments(df)
    st = extract_states(df)
    code_list = []
    for m in mn.Abbr:
        if len(st)==0:
            c = 0 #no state
        else:
            c = 1 #out of state
            for s in st.Abbr:
                if m == s:
                    c = 2 #in state
        code_list.append(c)
    #so the cases are
    #mn and st are not empty
    #for m in mn.Abbr:
    return(code_list)
        
